extends Spatial

onready var battle_container = $BattleContainer
onready var player_scene = preload("res://src/actors/player/impl/player.tscn")
onready var battle_1 = preload("res://src/game/battles/tutorial_battle/tutorial_battle.tscn")
onready var battle_2 = preload("res://src/game/battles/easy_battle/EasyBattle.tscn")
onready var battle_3 = preload("res://src/game/battles/main_battle_1/MainBattle1.tscn")
onready var battle_4 = preload("res://src/game/battles/main_battle_2/MainBattle2.tscn")
onready var battle_5 = preload("res://src/game/battles/main_battle_3/MainBattle3.tscn")
onready var battle_6 = preload("res://src/game/battles/main_battle_4/MainBattle4.tscn")
onready var battle_7 = preload("res://src/game/battles/main_battle_5/MainBattle5.tscn")
onready var battle_8 = preload("res://src/game/battles/main_battle_6/MainBattle6.tscn")
onready var battle_9 = preload("res://src/game/battles/main_battle_7/MainBattle7.tscn")
onready var player_start_position = $PlayerStartPosition
onready var camera = $Camera
onready var scene_manager = $SceneManager
onready var splash_screen = $Menus/SplashScreen
onready var start_screen = $Menus/StartScreen
onready var end_screen = $Menus/EndScreen

var player : Player
var battles = []
var index : int = 0

func _ready():
	splash_screen.connect("splash_screen_finished_playing", self, "_init_start_menu")
	start_screen.connect("start_button_pressed", self, "start_game")
	end_screen.connect("play_again_pressed", self, "end_game")
	
	battles = [
		battle_1,
		battle_2,
		battle_3,
		battle_4,
		battle_5,
		battle_6,
		battle_7,
		battle_8,
		battle_9
	]

func _init_start_menu():
	start_screen.visible = true

func _init_end_screen():
	end_screen.visible = true
	
func start_game():
	initialize_player()
	start_next_battle()
	AudioPlayer.play_level_music()

func end_game():
	end_screen.visible = false
	scene_manager.start_transition("fade_in", "fade_out", funcref(self, "reset_values"), funcref(self, "_init_start_menu"))


func initialize_next_battle():
	player.can_move = false
	battle_container.get_child(0).queue_free()
	
	if index < battles.size():
		scene_manager.start_transition("fade_in", "fade_out", funcref(self, "initialize_player"), funcref(self, "start_next_battle"))
	else:
		_init_end_screen()
	
func initialize_player():
	if player:
		player.queue_free()
		player = null
	player = player_scene.instance()
	player_start_position.add_child(player)
	player.connect("player_died", self, "_init_end_screen")
	camera.player = player

func reset_values():
	if player:
		camera.player = null
		player.queue_free()
		player = null
	
	var current_battle = battle_container.get_child(0)
	if is_instance_valid(current_battle):
		current_battle.queue_free()
	index = 0
		

func start_next_battle():
	var _battle : Battle = battles[index].instance()
	battle_container.add_child(_battle)
	_battle.connect("battle_won", self, "initialize_next_battle")
	
	player.can_move = true
	index += 1 

