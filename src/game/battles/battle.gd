extends Spatial
class_name Battle

onready var animation_player = $AnimationPlayer
onready var spawner_container = $SpawnerContainer


signal battle_won
signal battle_started

func _ready():
	animation_player.connect("animation_finished", self, "_on_animation_finished")
	spawner_container.connect("all_enemies_defeated", self, "_on_enemies_defeated")
	animation_player.play("begin_anim")

func _on_enemies_defeated():
	animation_player.play("end_anim")

func _on_animation_finished(anim : String):
	match anim:
		"begin_anim":
			start_battle()
	
		"end_anim" :
			end_battle()

func start_battle():
	emit_signal("battle_started")

func end_battle():
	emit_signal("battle_won")
	

