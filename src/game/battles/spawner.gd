extends Position3D
class_name Spawner

onready var timer = $Timer
onready var spawn_location = $SpawnLocation

export var enemy_to_spawn : PackedScene
export var spawn_burst_amount : int = 2
export var spawn_interval : int = 1
export var spawn_interval_limit : int = 1
export var stat_modifier : Resource

var total_enemies_to_spawn : int
var defeated_spawn_enemies : int = 0
var current_interval : int = 0
var rng = RandomNumberGenerator.new()

signal finished_spawning (enemy_amount)
signal enemies_defeated


func _ready():
	total_enemies_to_spawn = spawn_burst_amount * spawn_interval_limit
	rng.randomize()
	if !enemy_to_spawn || spawn_interval_limit <= 0:
		return
	
	spawn_enemies()
	
	timer.set_wait_time(spawn_interval)
	timer.start()
	timer.connect("timeout", self, "_on_timeout")
		

func spawn_enemies():
	for e in spawn_burst_amount:
		var x = rng.randf_range(0, 1) + global_translation.x
		var y = rng.randf_range(0, 1) + global_translation.y
		var z = rng.randf_range(0, 1) + global_translation.z
		var enemy : Enemy = enemy_to_spawn.instance()
		add_child(enemy)
		enemy.global_translation = spawn_location.global_translation		
		spawn_location.global_translation = Vector3(x,y,z)
		enemy.connect("defeated", self, "update_defeated_enemy_count")
		
		if stat_modifier:
			enemy.MOVEMENT_SPEED *= stat_modifier.movement_speed_multiplier
			enemy.stats.hp *= stat_modifier.health_point_multiplier
			enemy._on_health_updated(enemy.stats.hp)
			enemy.weapon.animation_player.playback_speed *= stat_modifier.attack_speed_multiplier
			enemy.weapon.animation_player.playback_speed *= stat_modifier.attack_speed_multiplier

		yield(get_tree().create_timer(0.4), "timeout")

func update_defeated_enemy_count():
	defeated_spawn_enemies += 1
	print(defeated_spawn_enemies)
	
	if defeated_spawn_enemies == total_enemies_to_spawn:
		emit_signal("enemies_defeated", defeated_spawn_enemies)

		

func _on_timeout():
	if current_interval < spawn_interval_limit-1:
		current_interval += 1
		spawn_enemies()
	else:
		timer.stop()
		emit_signal("finished_spawning", total_enemies_to_spawn)
