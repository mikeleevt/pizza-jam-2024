extends Spatial
class_name SpawnContainer

signal all_enemies_defeated

var enemies_to_defeat : int = 0
var enemies_defeated : int = 0

func _ready():
	var spawners = get_children()
	
	for spawner in spawners:
		if spawner is Spawner:
			var additional_enemy_count : int = spawner.spawn_burst_amount * spawner.spawn_interval_limit
			enemies_to_defeat += additional_enemy_count
			spawner.connect("enemies_defeated", self, "update_enemies_defeated")

func update_enemies_defeated(enemy_defeated : int):
	enemies_defeated += enemy_defeated
	
	if enemies_defeated == enemies_to_defeat:
		emit_signal("all_enemies_defeated")
		print("YOU WIN THE BATTLE")
	

