extends CanvasLayer

onready var texture_button = $CenterContainer/VBoxContainer/HBoxContainer/PlayAgainButton

signal play_again_pressed

func _ready():
	texture_button.connect("button_up", self, "_on_play_again_button_pressed")

func _on_play_again_button_pressed():
	emit_signal("play_again_pressed")
	visible = false
