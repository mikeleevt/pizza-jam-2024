extends Camera

 
export var min_X_bound = -5
export var max_X_bound = 10
export var max_Y_bound = 1
export var min_Y_bound = -1
export var min_Z_bound = 1
export var max_Z_bound = 20

var player : Player
var original_position : Vector3

func _ready():
	original_position = global_translation

func _physics_process(delta):
	if player:
		var offset : Vector3 = Vector3(0, 3, 5)
		global_translation = camera_bounds(player.global_translation.x, player.global_translation.y, player.global_translation.z) + offset
	else:
		global_translation = original_position

func camera_bounds(x, y, z):
	var pos_x = clamp(x, min_X_bound ,max_X_bound)
	var pos_y = clamp(y, min_Y_bound ,max_Y_bound)
	var pos_z = clamp(z, min_Z_bound ,max_Z_bound)
	
	return Vector3(pos_x, pos_y, pos_z)
