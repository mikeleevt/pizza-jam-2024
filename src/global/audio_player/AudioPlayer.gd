extends Node

onready var music_player = $MusicPlayer
onready var player_sfx = $PlayerSFX
onready var collectible_sfx = $CollectibleSFX
onready var level_sfx = $LevelSFX


var players = {}

func _ready():
	for audio_player in get_children():
		players[audio_player.name] = audio_player

func play_audio(audio_player_name : String, audio_stream : AudioStream, volume  : float):
	if players.has(audio_player_name):
		var player = players[audio_player_name] as AudioStreamPlayer
		
		player.set_volume_db(volume)
		player.set_stream(audio_stream)
		player.play()

# helpers

func play_level_music():
	AudioPlayer.play_audio("MusicPlayer", Constants.BATTLE_MUSIC, -2.0)
	

func play_title_music():
	AudioPlayer.play_audio("MusicPlayer", Constants.TITLE_MUSIC, -2.0)
	pass
func play_pick_up_SFX():
#	AudioPlayer.play_audio("PlayerSFX", Constants.PICKUP_COLLECTIBLE, -1.0)
	pass
func play_button_press_SFX():
#	AudioPlayer.play_audio("PlayerSFX", Constants.BUTTON_PRESS, -2.0)
	pass
func play_hit_SFX():
	AudioPlayer.play_audio("PlayerSFX", Constants.HIT_SFX, -2.0)
	pass
func play_jump_SFX():
	AudioPlayer.play_audio("PlayerSFX", Constants.JUMP_SFX, -9.0)
	pass
