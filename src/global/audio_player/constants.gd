extends Node

const INTRO = preload("res://assets/audio/music/Intro.ogg")
const TITLE_MUSIC = preload("res://assets/audio/music/StartScreen.ogg")
const BATTLE_MUSIC = preload("res://assets/audio/music/BattleTheme2.ogg")
const HIT_SFX = preload("res://assets/audio/sfx/hitHurt.wav")
const JUMP_SFX = preload("res://assets/audio/sfx/jump.wav")
