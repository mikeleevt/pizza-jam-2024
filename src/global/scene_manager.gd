extends Control

onready var animation_player = $AnimationPlayer

signal transition_halfway_done(end_transition, function)
signal transition_finished

func _ready():
	connect("transition_halfway_done", self, "_on_transition_halfway_done")

func start_transition(start_transition : String, end_transition : String, init : FuncRef, function : FuncRef):
	if animation_player.has_animation(start_transition):
		animation_player.play(start_transition)
		print('playing ' + start_transition)
		yield(animation_player,"animation_finished")		
		init.call_func()
		emit_signal("transition_halfway_done", end_transition, function)


func _on_transition_halfway_done(end_transition : String, function : FuncRef):
	print('playing ' + end_transition)
	
	if end_transition != "":
		
		animation_player.play(end_transition)
		emit_signal("transition_finished")
		yield(animation_player,"animation_finished")
		function.call_func()
