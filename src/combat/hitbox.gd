extends Area


signal got_hit (damage)

var is_active = true

# Called when the node enters the scene tree for the first time.
func _ready():
	connect("area_entered",self, "_on_area_entered")

func _on_area_entered(damage_source : Area) -> void:
	if damage_source.owner.is_in_group("damage_source"):
		var damage = damage_source.owner.damage
		emit_signal("got_hit", damage)

