extends Node
class_name Stats

export var hp : int
export var actor_name : String
export var sprite : Texture
export var type : String
onready var timer = $Timer

signal updated_health (damage)
signal hp_deleted

	
func take_damage(damage : int):
	hp -= damage
	
	if hp <= 0:
		emit_signal("hp_deleted")
		hp = 0
	
	emit_signal("updated_health", hp)


