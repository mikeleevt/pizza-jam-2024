extends Weapon

onready var area = $Area

func attack():
	area.set_collision_mask_bit(collision_layer_map[target_bit]-1, true)
	if animation_player.is_connected("animation_finished", self, "end_blast"):
		animation_player.connect("animation_finished", self, "end_blast")
	animation_player.play("blast")

func end_blast(anim_name : String):
	cleanup()
	animation_player.stop()

func enable():
	area.monitoring = true
	area.monitorable = true

func disable():
	area.monitoring = false
	area.monitorable = false
