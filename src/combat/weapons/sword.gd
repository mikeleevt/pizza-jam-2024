extends Weapon

onready var area = $Area

func attack():
	area.set_collision_mask_bit(collision_layer_map[target_bit]-1, true)
	if animation_player.is_connected("animation_finished", self, "end_swing"):
		animation_player.connect("animation_finished", self, "end_swing")
	animation_player.play("sword_swing")

func end_swing(anim_name : String):
	cleanup()

func enable():
	area.monitoring = true
	area.monitorable = true

func disable():
	
	area.monitoring = false
	area.monitorable = false
	
