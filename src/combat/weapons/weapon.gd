extends Spatial
class_name Weapon

onready var animation_player = $AnimationPlayer

export var damage : int = 5
export var knockback : int = 10

signal damaged_target ( target )
signal attack_started
signal attack_ended

var is_active : bool = false
var owner_map = {
	1 : 8,
	8 : 1
}
var collision_layer_map = {
	1 : 1,
	2 : 2,
	4 : 3,
	8 : 4,
	16 : 5,
	32 : 6,
	64 : 7,
	128 : 8
}

var parent : Node
var layer_bit : int
var target_bit : int

func init():
	parent = get_parent()
	enable()
	layer_bit = parent.owner.collision_layer
	target_bit = owner_map[parent.owner.collision_layer]
	is_active = true
	
	attack()

func attack():
	print('attack')

func disable():
	print('disable')	

func enable():
	print('enable')

func cleanup():
	is_active = false
	disable()

