extends Weapon

onready var area = $Area

func attack():
	area.set_collision_mask_bit(collision_layer_map[target_bit]-1, true)
	animation_player.connect("animation_finished", self, "swing")
	animation_player.play("swing")

func end_blast(anim_name : String):
	cleanup()
	animation_player.stop(false)

func enable():
	area.monitoring = true
	area.monitorable = true

func disable():
	area.monitoring = false
	area.monitorable = false
