extends Weapon
class_name Bow

onready var arrow = preload("res://src/combat/weapons/arrow.tscn")

func shoot(direction : Vector3):
	var _arrow : Arrow = arrow.instance()
	var tween = create_tween()
	
	_arrow.set_collision_mask_bit(collision_layer_map[1]-1, true)
	add_child(_arrow)
	_arrow.owner = self
	_arrow.global_translation = global_translation
	tween.tween_property(_arrow, "global_translation", direction, 2)
	yield(tween, "finished")
	_arrow.queue_free()
	

func end_blast(anim_name : String):
	cleanup()
	animation_player.stop()


