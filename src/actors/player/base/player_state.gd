extends State
class_name PlayerState

var player : Player

# Called when the node enters the scene tree for the first time.
func _ready():
	yield(owner, "ready")
	player = owner as Player
	assert(player != null)
	
