extends KinematicBody
class_name Player

onready var stats = $Stats
onready var state_machine = $StateMachine
onready var state_debug = $StateDebug
onready var weapon_container = $WeaponContainer
onready var hit_box = $HitBox
onready var health = $Health

export var init_weapon : PackedScene
export var movement_speed = 6

var original_movement_speed : int
var original_gravity : int
var can_move : bool = false
var weapon : Weapon
var velocity : Vector3 = Vector3.ZERO
var gravity : int = 20
var lock_facing_direction : bool = false

signal player_died

func _ready():
	original_gravity = gravity
	original_movement_speed = movement_speed
	state_machine.connect("transitioned", self, "_on_state_changed")
	stats.connect("updated_health", self, "_on_health_updated")
	stats.connect("hp_deleted", self, "_on_health_deleted")
	health.text = str(stats.hp)
	equip_weapon(init_weapon)

func _on_health_updated(updated_health : int):
	health.text = str(updated_health) 

func _on_health_deleted():
	can_move = false
	state_machine.transition_to("Death")
	hit_box.set_collision_layer_bit(0, false)
	hit_box.set_deferred("monitorable", false)
	emit_signal("player_died")

func _on_state_changed(state : String):
	state_debug.text = state

func get_movement_input():
	
	velocity.x = 0
	velocity.z = 0
	
	if !can_move:
		return
	
	if Input.is_action_pressed("move_left"):
		velocity.x = -movement_speed
		if !lock_facing_direction:
			scale.x = -1
	if Input.is_action_pressed("move_right"):
		velocity.x = movement_speed
		if !lock_facing_direction:
			scale.x = 1
	if Input.is_action_pressed("move_up"):
		velocity.z = -movement_speed/2
	if Input.is_action_pressed("move_down"):
		velocity.z = movement_speed/2
	
	if Input.is_action_just_pressed("attack"):
		state_machine.transition_to("Attack/Swing")
	
	if Input.is_action_just_pressed("dash"):
		state_machine.transition_to("Move/Dash")
	
	if Input.is_action_just_pressed("jump") and is_on_floor():
		velocity.y = 7
		AudioPlayer.play_jump_SFX()
	
	if Input.is_action_just_pressed("lock"):
		lock_facing_direction = !lock_facing_direction

func apply_gravity(delta):
	velocity.y -= gravity * delta

func take_damage(damage):
	state_machine.transition_to("Hurt")

func equip_weapon(_weapon : PackedScene):
	if weapon_container.get_child(0):
		weapon = weapon_container.get_child(0)
	elif _weapon:
		weapon = _weapon.instance()
		weapon_container.add_child(weapon)

func _physics_process(_delta: float) -> void:
	velocity = move_and_slide(velocity, Vector3.UP)
	apply_gravity(_delta)
	get_movement_input()


