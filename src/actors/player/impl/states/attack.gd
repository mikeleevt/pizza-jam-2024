extends PlayerState

#parent state to manage all attack based states

func enter(msg: Dictionary = {}):
	player.weapon.connect("damaged_target", self, "handle_hit")
	player.hit_box.connect("got_hit", self, "handle_damage")
	
func exit():
	player.weapon.disconnect("damaged_target", self, "handle_hit")
	player.hit_box.disconnect("got_hit", self, "handle_damage")

func handle_hit(target):
	pass

func handle_damage(amount):
	state_machine.transition_to("Hurt", {"damage" : amount})
