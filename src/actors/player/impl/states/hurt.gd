extends PlayerState

onready var timer = $Timer


func enter(msg : Dictionary = {}):
	AudioPlayer.play_hit_SFX()
	player.stats.take_damage(msg.damage)
	timer.connect("timeout", self, "_on_timeout")
	var tween = create_tween().set_loops(5)
	tween.tween_property(player, "visible", false, 0.1)
	tween.tween_property(player, "visible", true, 0.1)
	
	timer.start()

func _on_timeout():
	timer.disconnect("timeout", self, "_on_timeout")
	if player.is_on_floor() and player.velocity.x == 0:
		state_machine.transition_to("Move/Idle")
	elif player.is_on_floor() and player.velocity.x > 0:
		state_machine.transition_to("Move/Run")
	elif abs(player.velocity.y) > 0:
		state_machine.transition_to("Move/Air")
