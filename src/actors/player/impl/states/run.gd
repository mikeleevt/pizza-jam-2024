extends PlayerState

func enter(msg : Dictionary = {}):
	get_parent().enter(msg)

func exit() -> void:
	get_parent().exit()

func physics_update(delta : float):
	if player.velocity.x == 0 and player.velocity.z == 0  and player.is_on_floor():
		state_machine.transition_to("Move/Idle")
	elif !player.is_on_floor():
		state_machine.transition_to("Move/Air")

