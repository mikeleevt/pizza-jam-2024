extends PlayerState

func enter(msg : Dictionary = {}):
	get_parent().enter(msg)

func exit() -> void:
	get_parent().exit()

func physics_update(delta : float):
	if abs(player.velocity.x) > 0 || abs(player.velocity.z) > 0:
		if player.is_on_floor():
			state_machine.transition_to("Move/Run")
	elif !player.is_on_floor():
			state_machine.transition_to("Move/Air")
		
	

