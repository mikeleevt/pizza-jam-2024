extends PlayerState

#parent state to manage all movement based states

func enter(msg: Dictionary = {}):
	player.hit_box.connect("got_hit", self, "handle_damage")
	
func exit():
	if player.hit_box.is_connected("got_hit", self, "handle_damage"):
		player.hit_box.disconnect("got_hit", self, "handle_damage")

func handle_damage(amount):
	state_machine.transition_to("Hurt", {"damage" : amount})
