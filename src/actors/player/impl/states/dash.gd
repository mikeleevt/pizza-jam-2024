extends PlayerState

onready var timer = $Timer

func enter(msg : Dictionary = {}):
	if !timer.is_stopped():
		exit()
		return
	get_parent().enter(msg)
	timer.start()
	timer.connect("timeout", self, "_on_timeout")
	player.movement_speed *= 2

func exit() -> void:
	get_parent().exit()

func _on_timeout():
	player.movement_speed = player.original_movement_speed
	timer.disconnect("timeout", self, "_on_timeout")
	if player.is_on_floor():
		state_machine.transition_to("Move/Idle")
	else:
		state_machine.transition_to("Move/Air")
