extends PlayerState

onready var timer : Timer = $Timer

func enter(msg  = {}):
	get_parent().enter(msg)
	player.weapon.init()
	timer.connect("timeout", self, "_on_timeout")
	timer.start()

func exit():
	get_parent().exit()
	timer.stop()
	timer.disconnect("timeout", self, "_on_timeout")
	
func _on_timeout():
	timer.stop()
	timer.disconnect("timeout", self, "_on_timeout")
	if player.is_on_floor():
		state_machine.transition_to("Move/Idle")
	else:
		state_machine.transition_to("Move/Air")
		
