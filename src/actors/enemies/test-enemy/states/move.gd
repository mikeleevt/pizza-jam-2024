extends EnemyState

func enter(msg: Dictionary = {}):
	enemy.stats.connect("taken_damage", self, "handle_damage")
	enemy.detection_area.connect("area_entered", self, "detected_player")
	enemy.hit_box.connect("got_hit", self, "handle_damage")
	
func exit():
	enemy.stats.disconnect("taken_damage", self, "handle_damage")
	enemy.detection_area.disconnect("area_entered", self, "detected_player")
	enemy.hit_box.disconnect("got_hit", self, "handle_damage")

func detected_player(detected_object):
	if detected_object.owner is Player:
		state_machine.transition_to("Attack/Follow", {"player" : detected_object.owner})

func handle_damage(amount):
	state_machine.transition_to("Hurt", {"damage" : amount})
