extends EnemyState

var detected_player : Player

func enter(msg : Dictionary = {}):
	enemy.hit_box.connect("got_hit", self, "handle_damage")
	if !enemy.detection_area.is_connected("area_exited", self, "player_lost"):
		enemy.detection_area.connect("area_exited", self, "player_lost")
	if msg.has("player"):
		detected_player = msg.player

func exit():
	enemy.hit_box.disconnect("got_hit", self, "handle_damage")
	if enemy.detection_area.is_connected("area_exited",self, "player_lost"):
		enemy.detection_area.disconnect("area_exited", self, "player_lost")

func player_lost(player):
	detected_player = null
	state_machine.transition_to("Move/Idle")

func handle_damage(amount):
	state_machine.transition_to("Hurt", {"damage" : amount, "player" : detected_player})
