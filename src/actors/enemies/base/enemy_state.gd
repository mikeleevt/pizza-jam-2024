extends State
class_name EnemyState

var enemy : Enemy

# Called when the node enters the scene tree for the first time.
func _ready():
	yield(owner, "ready")
	enemy = owner as Enemy
	assert(enemy != null)
	
