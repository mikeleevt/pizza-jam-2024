extends Resource
class_name StatModifier

export var health_point_multiplier : int = 2
export var movement_speed_multiplier : int = 2
export var attack_speed_multiplier : float = 2
export var knockback_multiplier : float = 1
