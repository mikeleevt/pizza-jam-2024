extends KinematicBody
class_name Enemy

onready var detection_area = $DetectionArea
onready var stats = $Stats
onready var state_debug = $StateDebug
onready var state_machine = $StateMachine
onready var weapon_container = $Pivot/WeaponContainer
onready var ray_cast_3d = $Pivot/RayCast
onready var hit_box = $HitBox
onready var health = $Health
onready var pivot = $Pivot
onready var animation_player = $AnimationPlayer
onready var sprite_3d = $Pivot/Sprite3D

export var init_weapon : PackedScene
export var MOVEMENT_SPEED = 50
export var GRAVITY = 20
export var knockback_amount : float = 2
export var stun_duration : float = 0.2

enum directions {
	RIGHT,
	LEFT
}

var current_facing_direction = directions.RIGHT
var velocity : Vector3 = Vector3.ZERO
var weapon : Weapon

signal defeated

func _ready():
	state_machine.connect("transitioned", self, "_on_state_changed")
	stats.connect("hp_deleted", self, "_on_hp_depleted")
	stats.connect("updated_health", self, "_on_health_updated")
	health.text = str(stats.hp)
	equip_weapon(init_weapon)

func _on_health_updated(updated_health : int):
	health.text = str(updated_health) 

func _on_state_changed(state: String):
	state_debug.text = state

func apply_gravity(delta):
	velocity.y -= GRAVITY * delta

func update_facing_direction():

		
	if velocity.x < -0.1:
		current_facing_direction = directions.LEFT
		pivot.scale.x = -1
		sprite_3d.flip_h = true
	elif velocity.x > 0.1:
		pivot.scale.x = 1
		sprite_3d.flip_h = false
		current_facing_direction = directions.RIGHT

func take_damage(damage):
	state_machine.transition_to("Hurt")
	

func _physics_process(delta):
	apply_gravity(delta)
	velocity = move_and_slide(velocity, Vector3.UP)
	update_facing_direction()

func _on_hp_depleted():
	emit_signal("defeated")
	queue_free()
func equip_weapon(_weapon : PackedScene):
	if _weapon:
		weapon = _weapon.instance()
		weapon_container.add_child(weapon)
