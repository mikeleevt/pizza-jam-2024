extends EnemyState

func physics_update(delta : float):
	pass 

static func get_move_direction() -> Vector3:
	return Vector3.ZERO

func enter(msg: Dictionary = {}):
	enemy.detection_area.connect("area_entered", self, "detected_player")
	enemy.hit_box.connect("got_hit", self, "handle_damage")
	
func exit():
	if enemy.detection_area.is_connected("area_entered", self, "detected_player"):
		enemy.detection_area.disconnect("area_entered", self, "detected_player")
	enemy.hit_box.disconnect("got_hit", self, "handle_damage")

func detected_player(detected_object):
	if detected_object.owner is Player:
		state_machine.transition_to("Attack/Follow", {"player" : detected_object.owner})

func handle_damage(amount):
	state_machine.transition_to("Hurt", {"damage" : amount})
