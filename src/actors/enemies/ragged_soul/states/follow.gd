extends EnemyState

export var distance_to_attack : Vector2

var detected_player : Player
var offset : Vector3  = Vector3(5,0,0)

func enter(msg : Dictionary = {}):
	get_parent().enter(msg)
	detected_player = msg.player
	enemy.velocity = Vector3.ZERO

func exit() -> void:
	enemy.velocity = Vector3.ZERO
	get_parent().exit()

func physics_update(delta):
	enemy.ray_cast_3d.cast_to = detected_player.global_translation - enemy.global_translation
	enemy.animation_player.play("Run")
	if abs(enemy.ray_cast_3d.cast_to.x) < 5 and abs(enemy.ray_cast_3d.cast_to.z) < 5:
		if !enemy.weapon:
			return
		state_machine.transition_to("Attack/Shoot", {"player": detected_player})	
	
	if enemy.is_on_floor():
		enemy.velocity =  (detected_player.global_translation - enemy.global_translation).normalized() * enemy.MOVEMENT_SPEED * delta
		enemy.velocity.y = enemy.GRAVITY*delta

