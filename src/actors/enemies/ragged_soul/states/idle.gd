extends EnemyState

func enter(msg : Dictionary = {}):
	get_parent().enter(msg)

func exit() -> void:
	get_parent().exit()

func physics_update(delta : float):
	enemy.velocity.x = 0
	enemy.velocity.z = 0
	if abs(enemy.velocity.x) > 0 || abs(enemy.velocity.z) > 0:
		if enemy.is_on_floor():
			state_machine.transition_to("Move/Idle")
	elif !enemy.is_on_floor():
			state_machine.transition_to("Move/Air")
	
