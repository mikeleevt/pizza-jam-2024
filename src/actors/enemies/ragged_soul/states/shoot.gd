extends EnemyState

var detected_player : Player
var current_player_position : Vector3

onready var timer = $Timer

func enter(msg : Dictionary = {}):
	get_parent().enter(msg)
	detected_player = msg.player
	timer.connect("timeout", self, "_on_timeout")


func exit():
	timer.stop()
	timer.disconnect("timeout", self, "_on_timeout")
	
	get_parent().exit()

func physics_update(delta):
	enemy.velocity.x = 0
	enemy.velocity.z = 0
	enemy.ray_cast_3d.cast_to = detected_player.global_translation - enemy.global_translation
	
	var distance_to_player : float = 6
	
	if abs(enemy.ray_cast_3d.cast_to.x) < distance_to_player and abs(enemy.ray_cast_3d.cast_to.z) < distance_to_player:
		move_away_from_player(detected_player, delta)
		if timer.is_stopped() || timer.is_paused():
			timer.wait_time = 1
			timer.start()
	elif (abs(enemy.ray_cast_3d.cast_to.x) > 3 and abs(enemy.ray_cast_3d.cast_to.z) > 3) || abs(enemy.ray_cast_3d.cast_to.x) > 5 || abs(enemy.ray_cast_3d.cast_to.z) > 5:
		if get_parent().detected_player:
			state_machine.transition_to("Attack/Follow", {"player" : detected_player})
		else:
			state_machine.transition_to("Move/Idle")

func _on_timeout():
	shoot_arrow()
	timer.wait_time = 2

	
func shoot_arrow():
	if enemy.weapon:
		enemy.weapon.shoot(detected_player.global_translation)

func move_away_from_player(player : Player, delta):
	if enemy.is_on_floor():
		enemy.velocity =  -(detected_player.global_translation - enemy.global_translation).normalized() * enemy.MOVEMENT_SPEED * delta
		enemy.velocity.y = enemy.GRAVITY*delta

