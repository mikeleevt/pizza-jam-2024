extends EnemyState

onready var timer = $Timer


func enter(msg : Dictionary = {}):
	if !msg.has("player"):
		state_machine.transition_to("Move/Idle")
		exit()
		return
	enemy.stats.take_damage(msg.damage)
	timer.connect("timeout", self, "_on_timeout", [msg.player])
	
	var tween = create_tween()
	var facing_direction : int = enemy.current_facing_direction
	var direction : int = get_knockback_direction(facing_direction)
	
	timer.wait_time = enemy.stun_duration
	tween.tween_property(enemy, "global_translation:x", enemy.velocity.x + enemy.knockback_amount * direction, timer.wait_time).set_ease(Tween.EASE_OUT)
	timer.start()

func _on_timeout(player : Player):
	timer.disconnect("timeout", self, "_on_timeout")
	if player:
		state_machine.transition_to("Attack/Follow", {"player" : player})
	elif enemy.is_on_floor():
		state_machine.transition_to("Move/Idle")
	else:
		state_machine.transition_to("Move/Air")



func get_knockback_direction(entity_facing_direction : int):
	match entity_facing_direction:
		enemy.directions.LEFT:
			return -1
		enemy.directions.RIGHT:
			return 1

