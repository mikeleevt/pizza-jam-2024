extends EnemyState

var detected_player : Player

func enter(msg: Dictionary = {}):
	enemy.detection_area.connect("area_entered", self, "detected_player")
	enemy.hit_box.connect("got_hit", self, "handle_damage")
	if msg.has("player"):
		detected_player = msg.player
func exit():
	enemy.detection_area.disconnect("area_entered", self, "detected_player")
	enemy.hit_box.disconnect("got_hit", self, "handle_damage")

func detected_player(detected_object):
	if detected_object.owner is Player:
		state_machine.transition_to("Attack/Follow", {"player" : detected_object.owner})

func handle_damage(amount):
	state_machine.transition_to("Hurt", {"damage" : amount, "player" : detected_player})
