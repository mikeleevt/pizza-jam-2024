extends EnemyState

onready var timer : Timer = $Timer

func enter(msg  = {}):
	if !timer.is_stopped():
		return
	get_parent().enter(msg)
	enemy.weapon.init()
	timer.connect("timeout", self, "_on_timeout")
	timer.start()

func physics_update(delta):
	enemy.velocity.x = 0
	enemy.velocity.z = 0

func _on_timeout():
	get_parent().exit()
	timer.disconnect("timeout", self, "_on_timeout")
	if get_parent().detected_player:
		state_machine.transition_to("Attack/Follow", {"player" : get_parent().detected_player})
	else:
		state_machine.transition_to("Move/Idle")
