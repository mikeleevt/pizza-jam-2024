extends EnemyState

export var distance_to_attack : Vector2

var detected_player : Player

func enter(msg : Dictionary = {}):
	get_parent().enter(msg)
	detected_player = msg.player
	enemy.velocity = Vector3.ZERO

func exit() -> void:
	enemy.velocity = Vector3.ZERO
	get_parent().exit()

func physics_update(delta):
	if enemy.ray_cast_3d.is_colliding():
		if !enemy.weapon:
			return
		state_machine.transition_to("Attack/Stab")	
	
	if enemy.is_on_floor():
		enemy.velocity =  (detected_player.global_translation - enemy.global_translation).normalized() * enemy.MOVEMENT_SPEED * delta
		enemy.velocity.y = enemy.GRAVITY*delta

