extends EnemyState

func enter(msg : Dictionary = {}):
	get_parent().enter(msg)

func exit() -> void:
	get_parent().exit()

func physics_update(_delta : float) -> void:
	if enemy.is_on_floor():
		state_machine.transition_to("Move/Idle")
