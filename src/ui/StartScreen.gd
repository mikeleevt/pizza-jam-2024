extends CanvasLayer

onready var play_button = $CenterContainer/VBoxContainer/PlayButton

signal start_button_pressed

func _ready():
	play_button.connect("button_up", self, "_on_play_button_pressed")

func _on_play_button_pressed():
	emit_signal("start_button_pressed")
	visible = false
