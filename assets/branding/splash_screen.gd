extends CanvasLayer

onready var animation_player = $AnimationPlayer

export var next_scene : PackedScene

signal splash_screen_finished_playing

func _ready():
	animation_player.connect("animation_finished", self, "clean_up_splash_screen")
	yield(get_tree().create_timer(1),"timeout")
	AudioPlayer.play_title_music()

func clean_up_splash_screen(anim_name : String):
	emit_signal("splash_screen_finished_playing")	
	queue_free()
